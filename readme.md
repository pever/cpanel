## Laravel Admin Panel

Laravel 4 package used to provide an admin panel with user, groups and permissions management.
This package is currently under active development.

### Features
* Cartalyst Sentry package
* Anahkiasen Former package
* Twitter Bootstrap 2.3.1
* Font-awesome 3.2.0
* Users, Groups and Permissions out of the box.
* Base controller for admin panel development
* Most of the views can be replaced by your own in the config file

### Installation
Begin by installing this package through Composer. Edit your project's `composer.json` file to require `stevemo/cpanel`.

```javascript
{
    "require": {
        "manage/users": "dev-master"
    }
}
```

Update your packages with `composer update` or install with `composer install`.

You need to add the following service provider. 
Open `app/config/app.php`, and add a new items to the providers array.

```php
'Former\FormerServiceProvider',
'Cartalyst\Sentry\SentryServiceProvider',
'Stevemo\Cpanel\CpanelServiceProvider',
'Stevemo\Cpanel\Providers\EventServiceProvider',
```

Then add the following Class Aliases
```php
'Former' => 'Former\Facades\Former',
'Sentry' => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
```

Finally run the following command in the terminal. `php artisan cpanel:install`
This will publish the config files for Cartalyst/Sentry, Anahkiasen/Former and Stevemo/Cpanel also it will run the migration.

To create a user simply do `php artisan cpanel:user`

Done! Just go to [http://localhost/admin](http://localhost/admin) to access the admin panel.

### Configure Email
* configure app/config/mail.php


### Configure Social

Add your application configuration to a `.env` file in the root of your
project. **Make sure the `.env` file is added to your `.gitignore` so it is not
checked-in the code**

```php
FACEBOOK_CLIENT_ID=dotenv
FACEBOOK_CLIENT_SECRET=souper_seekret_key

GOOGLE_CLIENT_ID=dotenv
GOOGLE_CLIENT_SECRET=souper_seekret_key
```

### Load .env
You can then load `.env` in your application with a single line on `app/bootstrap/start.php`:

```php
if (file_exists(__DIR__.'/../.env'))
{
    Dotenv::load(__DIR__.'/../');
}

//changue production
$env = $app->detectEnvironment(function () {
    return getenv('APP_ENV') ?: 'production';
});
```

### Missing
* Send Activation code by email when user register
* Password reset/reminder
* unit test… started reading Laravel Testing decoded ;-)
* Documentation