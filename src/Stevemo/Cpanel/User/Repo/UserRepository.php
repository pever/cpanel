<?php namespace Stevemo\Cpanel\User\Repo;

use Carbon\Carbon;
use Cartalyst\Sentry\Sentry;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Users\UserNotFoundException as SentryUserNotFoundException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Illuminate\Events\Dispatcher;
use Cartalyst\Sentry\Users\UserInterface;
use Cartalyst\Sentry\Users\UserAlreadyActivatedException;
use Mychef\Chef\Repo\Chef;
use Stevemo\Cpanel\Social\Repo\SocialInterface;
use Mychef\ProfileImage\Repo\ProfileImageInterface;
use Jenssegers\Agent\Agent;
use Mychef\LoginDevice\Repo\LoginDeviceInterface;
use Mychef\UserLoginDevice\Repo\UserLoginDeviceInterface;
use Lang;
use Request;

/**
 * Class UserRepository
 * @package Stevemo\Cpanel\User\Repo
 */
class UserRepository implements CpanelUserInterface
{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    public static $TYPE_FACEBOOK = 'facebook';
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    public static $TYPE_GOOGLE = 'google';
    /**
     * @var \Cartalyst\Sentry\Sentry
     */
    protected $sentry;

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $event;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var SocialInterface
     */
    protected $socialRepo;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $imageRepo;

    /**
     * @var LoginDeviceInterface
     */
    protected $loginDevice;

    /**
     * @var UserLoginDeviceInterface
     */
    protected $userLoginDevice;


    /**
     * @param Sentry $sentry
     * @param Dispatcher $event
     * @param SocialInterface $socialRepo
     * @param ProfileImageInterface $imageInterface
     * @param LoginDeviceInterface $loginDevice
     * @param UserLoginDeviceInterface $userLoginDevice
     */
    public function __construct(
        Sentry $sentry,
        Dispatcher $event,
        SocialInterface $socialRepo,
        ProfileImageInterface $imageInterface,
        LoginDeviceInterface $loginDevice,
        UserLoginDeviceInterface $userLoginDevice
    ){
        $this->sentry = $sentry;
        $this->event = $event;
        $this->socialRepo = $socialRepo;
        $this->imageRepo = $imageInterface;
        $this->loginDevice = $loginDevice;
        $this->userLoginDevice = $userLoginDevice;
    }

    /**
     * Activate a user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $id
     *
     * @return bool
     */
    public function activate($id)
    {
        $user = $this->findById($id);

        try {
            if ($user->attemptActivation($user->getActivationCode())) {
                $this->event->fire('users.activate', array($user));
                return true;
            }
        } catch (UserAlreadyActivatedException $e) {
        }
        return false;
    }

    /**
     * Attempts to authenticate the given user
     * according to the passed credentials.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param array $credentials
     * @param bool $remember
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function authenticate(array $credentials, $remember = false)
    {
        $user = $this->sentry->authenticate($credentials, $remember);
        $this->event->fire('users.login', array($user));
        return $user;
    }

    /**
     * Check to see if the user is logged in and activated, and hasn't been banned or suspended.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return bool
     */
    public function check()
    {
        // TODO-Stevemo: Implement check() method.
    }

    /**
     * Create a new user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param array $credentials
     * @param bool $activate
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function create(array $credentials, $activate = false)
    {
        $user = $this->storeUser($credentials, $activate);
        $this->event->fire('users.create', array($user));

        return $user;
    }

    /**
     * De activate a user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $id
     *
     * @return bool
     */
    public function deactivate($id)
    {
        $user = $this->findById($id);
//        $user->activated = 0;
        $user->activated_at = null;
        $user->save();
        $this->event->fire('users.deactivate', array($user));
        return true;
    }

    /**
     * Delete the user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $user = $this->findById($id);
        $eventData = $user;
        $user->delete();
        $this->event->fire('users.delete', array($eventData));
    }

    /**
     * Returns an all users.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return array
     */
    public function findAll()
    {
        return $this->sentry->findAllUsers();
    }

    /**
     * Finds a user by the given user ID.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $id
     *
     * @throws UserNotFoundException
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function findById($id)
    {
        try {
            return $this->sentry->findUserById($id);
        } catch (SentryUserNotFoundException $e) {
            throw new UserNotFoundException($e->getMessage());
        }
    }

    /**
     * Find a given user by the login attribute
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $login
     *
     * @throws UserNotFoundException
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function findByLogin($login)
    {
        try {
            return $this->sentry->findUserByLogin($login);
        } catch (SentryUserNotFoundException $e) {
            throw new UserNotFoundException($e->getMessage());
        }
    }

    /**
     * Logs the current user out.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->sentry->getUser();
        $this->sentry->logout();
        $this->event->fire('users.logout', array($user));
    }

    /**
     * Returns the current user being used by Sentry, if any.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->sentry->getUser();
    }

    /**
     * Get the throttle provider for a given user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $id
     *
     * @throws UserNotFoundException
     *
     * @return \Cartalyst\Sentry\Throttling\ThrottleInterface
     */
    public function getUserThrottle($id)
    {
        try {
            return $this->sentry->findThrottlerByUserId($id);
        } catch (SentryUserNotFoundException $e) {
            throw new UserNotFoundException($e->getMessage());
        }
    }

    /**
     * Returns an empty user object.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \StdClass
     */
    public function getEmptyUser()
    {
        return $this->sentry->getEmptyUser();
    }

    /**
     * Registers a user by giving the required credentials
     * and an optional flag for whether to activate the user.
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param array $credentials
     * @param bool $activate
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function register(array $credentials, $activate = false)
    {
        $user = $this->storeUser($credentials, $activate);
        $this->event->fire('users.register', array($user));
        return $user;
    }

    /**
     *  Reset a given user password
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $code
     * @param $password
     *
     * @throws UserNotFoundException
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function resetPassword($code, $password)
    {
        try {
            $user = $this->sentry->findUserByResetPasswordCode($code);
            $user->password = $password;
            $user->save();

            $this->event->fire('users.password.reset', array($user));

            return $user;
        } catch (SentryUserNotFoundException $e) {
            throw new UserNotFoundException($e->getMessage());
        }
    }

    /**
     * Update user information
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param       $id
     * @param array $attributes
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function update($id, array $attributes)
    {
        $user = $this->findById($id);

        $user->first_name = $attributes['first_name'];
        $user->last_name = $attributes['last_name'];
        $user->email = $attributes['email'];
        $user->permissions = $attributes['permissions'];

        if (array_key_exists('password', $attributes)) {
            $user->password = $attributes['password'];
        }

        $user->save();

        if (array_key_exists('groups', $attributes)) {
            $this->syncGroups($attributes['groups'], $user);
        }

        $this->event->fire('users.update', array($user));

        return $user;
    }

    /**
     * Update permissions for a given user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param int $id
     * @param array $permissions
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function updatePermissions($id, array $permissions)
    {
        $user = $this->findById($id);
        $user->permissions = $permissions;
        $user->save();
        $this->event->fire('users.permissions.update', array($user));
        return $user;
    }

    /**
     * @param $id
     */
    public function updateOrGetCodeActivation($id)
    {
        $user = $this->findById($id);
        $this->event->fire('users.sendCode', array($user));
    }
    /**
     *
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param $id
     * @param $status
     *
     * @throws \BadMethodCallException
     *
     * @return void
     */
    public function updateThrottleStatus($id, $status)
    {
        $throttle = $this->getUserThrottle($id);
        call_user_func(array($throttle, $status));
        $this->event->fire('users.' . $status, array($throttle));
    }


    /**
     * Create user into storage
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param array $credentials
     * @param bool $activate
     *
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    protected function storeUser(array $credentials, $activate = false)
    {
        $cred = array(
            'first_name' => $credentials['first_name'],
            'last_name' => $credentials['last_name'],
            'email' => $credentials['email'],
            'password' => $credentials['password'],
        );

        if (array_key_exists('permissions', $credentials)) {
            $cred['permissions'] = $credentials['permissions'];
        }

        $user = $this->sentry->register($cred, $activate);

        $user->getActivationCode();

        if (array_key_exists('groups', $credentials)) {
            $this->syncGroups($credentials['groups'], $user);
        }

        return $user;
    }

    /**
     * Add groups to a user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param array $groups
     * @param \Cartalyst\Sentry\Users\UserInterface $user
     */
    protected function syncGroups(array $groups, UserInterface $user)
    {
        $user->groups()->detach();
        $user->groups()->sync($groups);
    }


    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     *
     * @param $userId
     * @param $code
     * @return bool
     */
    public function activateByActivationCode($userId, $code)
    {
        $user = $this->sentry->findUserById($userId);

        try {
            if (is_null($user->activated_at)) {

                if ($code == $user->activation_code)
                {
                    $user->activation_code = null;
                    $user->activated       = true;
                    $user->activated_at    = Carbon::now();
                    $user->save();
                    $this->event->fire('users.activate', array($user));
                    return true;
                }
//                if ($user->attemptActivation($code)) {
//                    $this->event->fire('users.activate', array($user));
//                    return true;
//                }
            }
            return false;
        } catch (UserAlreadyActivatedException $e) {
        }

        return false;
    }

    /**
     * @param $user
     * @param array $credentials
     * @param bool $activate
     * @return UserInterface
     */
    public function registerUserFacebook($user, array $credentials, $activate = false)
    {
        try {
            $user = $this->storeUserFacebook($credentials, $user, $activate);
            $this->event->fire('users.register', array($user));
            return $user;
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $googleUser
     * @param array $credentials
     * @param bool $activate
     * @return UserInterface
     */
    public function registerUserGoogle($googleUser, array $credentials, $activate = false)
    {
        try {
            $user = $this->storeUserGoogle($credentials, $googleUser, $activate);
            $this->event->fire('users.register', array($user));
            return $user;
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }


    /**
     * @param $credentials
     * @param $fbUser
     * @param $activate
     * @return UserInterface
     */
    private function storeUserFacebook($credentials, $fbUser, $activate)
    {
        try {
            $cred = array(
                'first_name' => $credentials['first_name'],
                'last_name' => $credentials['last_name'],
                'email' => $credentials['email'],
                'gender' => $fbUser->getGender(),
                'picture' => "https://graph.facebook.com/" . $fbUser->getId() . "/picture?type=square&height=320&width=320",
                'password' => $credentials['password'],
                'active' => true
            );

            if (array_key_exists('permissions', $credentials)) {
                $cred['permissions'] = $credentials['permissions'];
            }

            $user = $this->sentry->register($cred, $activate);

            // Activate User
            $this->activateUser($user);

            // Save Slug
            $this->saveSlug($user);

            // Save Chef
            $this->saveChef($user);

            $user->getActivationCode();

            if (array_key_exists('groups', $credentials)) {
                $this->syncGroups($credentials['groups'], $user);
            }

            $social = $this->storeSocial(array(
                'user_id' => $user->getId(),
                'type' => self::$TYPE_FACEBOOK,
                'picture' => "https://graph.facebook.com/" . $fbUser->getId() . "/picture?type=square&height=320&width=320",
                'social_id' => $fbUser->getId(),
                'url' => $fbUser->getLink()
            ));

            $this->storeProfileImage($social);

            return $user;

        } catch (UserExistsException $e) {
            $message = \Lang::get('users::users.user_exists', array('email' => $credentials['email']));
            throw new \Exception ($message);
        } catch (\Exception $e ){
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $cred
     * @return mixed
     */
    private function storeSocial($cred)
    {
        return $this->socialRepo->create($cred);
    }

    /**
     * @param $credentials
     * @param $googleUser
     * @param $activate
     * @return UserInterface
     */
    private function storeUserGoogle($credentials, $googleUser, $activate)
    {
        try {
            $cred = array(
                'first_name' => $credentials['first_name'],
                'last_name' => $credentials['last_name'],
                'email' => $credentials['email'],
                'gender' => $googleUser->getGender(),
                'password' => $credentials['password'],
                'picture' => $googleUser->picture
            );

            if (array_key_exists('permissions', $credentials)) {
                $cred['permissions'] = $credentials['permissions'];
            }

            $user = $this->sentry->register($cred, $activate);

            // Activate User
            $this->activateUser($user);

            // Save Slug
            $this->saveSlug($user);

            // Save Chef
            $this->saveChef($user);

            $user->getActivationCode();

            if (array_key_exists('groups', $credentials)) {
                $this->syncGroups($credentials['groups'], $user);
            }

            $social = $this->storeSocial(array(
                'user_id' => $user->getId(),
                'type' => self::$TYPE_GOOGLE,
                'social_id' => $googleUser->getId(),
                'picture' => $googleUser->picture,
                'url' => $googleUser->getLink()
            ));

            $this->storeProfileImage($social);

            return $user;

        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * check to completed data user
     *
     * @return mixed
     */
    public function checkCompleted()
    {
        if (is_null($this->sentry->getUser()->completed_at)) {
//            return false;
        }
        return true;
    }


    /**
     * @param $social
     */
    public function storeProfileImage($social)
    {
        $attributes = array(
            'user_id' => $social->user_id,
            'social_id' => $social->id,
            'image' => $social->picture,
            'type' => $social->type,
            'default' => 1
        );

        $this->imageRepo->saveSocial($attributes);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        try {
            $user = $this->sentry->findUserByLogin($email);
            if (is_null($user)) {
                throw new \Exception(Lang::get('users::users.email_not_register', array('email'=>$email)));
            }

            if (is_null($user->social)) {
                throw new \Exception(Lang::get('users::users.social_not_found', array('email'=>$email)));
            }
            return $user;

        } catch (UserNotFoundException $e) {
            throw new \Exception(Lang::get('users::users.not_found', array('email'=>$email)));
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $user
     * @return bool
     */
    public function login($user)
    {
        try {
            $this->userIsDeactivated($user);

            $dataConexion = $this->getDataConexion($user->id);

            $loginDevice = $this->loginDevice->findOrCreate($dataConexion);

            if (!$loginDevice) {
                throw new \Exception (Lang::get('users::users.device_not_allowed'));
            }
            $this->sentry->login($user);

        } catch (UserNotActivatedException $e) {
            throw new \Exception (Lang::get('users::users.not_activated', array('link'=>route(\Lang::get('users::routes.register')))));
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $user_id
     * @return array
     */
    public function getDataConexion($user_id)
    {
        $agent = new Agent();

        $platform = $agent->platform();
        $versionPlatform = $agent->version($platform);

        $browser = $agent->browser();
        $versionBrowser = $agent->version($browser);
        $languages = $agent->languages();

        $device = $agent->device();

        $type_platform = '';

        if ($agent->isDesktop()) {
            $type_platform = 'desktop';
        } elseif ($agent->isMobile()) {
            $type_platform = 'mobile';
        } elseif ($agent->isTablet()) {
            $type_platform = 'tablet';
        } elseif ($agent->isRobot()) {
            $type_platform = 'robot';
        }

        $attributes = array(
            'platform' => $platform,
            'platform_version' => $versionPlatform,
            'browser' => $browser,
            'browser_version' => $versionBrowser,
            'browser_languages' => implode(",", $languages),
            'device' => $device,
            'type_platform' => $type_platform
        );

        $ip = Request::getClientIp();

        $attributesLogin = array(
            'user_id' => $user_id,
            'ip' => $ip
        );

        return array($attributes, $attributesLogin);
    }

    /**
     * @param $id
     */
    public function updateActivationCode($id)
    {
        $user = $this->findById($id);
        $user->getActivationCode();
    }

    /**
     * @param $user
     */
    public function userIsDeactivated($user)
    {
        if ($user->activated == 0 && !is_null($user->activated_at))
//        if (is_null($user->activation_code) && is_null($user->activated_at))
        {
            $user->activated_at = Carbon::now();
            $user->activated = 1;
            $user->save();
        }
    }

    /**
     * @param $user
     */
    public function saveSlug($user)
    {
        $title = str_replace(' ','',$user->first_name).' '.str_replace(' ','',$user->last_name);
        $title = \Str::slug($title,'-');
        $title = $this->verifSlug($title, $user->id);
        $user->slug = $title;
        $user->save();
    }

    /**
     * @param $user
     */
    public function activateUser($user)
    {
        $user->activated = true;
        $user->save();
    }

    /**
     * @param $user
     */
    public function saveChef($user)
    {
        Chef::create(array(
            'user_id' => $user->id,
            'full_name' => $user->full_name,
            'picture' => $user->picture
        ));
    }

    /**
     * @param $title
     * @param $user_id
     * @return string
     */
    public function verifSlug($title, $user_id)
    {
        $usuario = \Mychef\User\Repo\User::where('slug',$title)->first();

        if (isset($usuario)) {
            $title = $title.$user_id;
        }
        
        return $title;
    }
}