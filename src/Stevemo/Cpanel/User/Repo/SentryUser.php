<?php
/**
 * Created by PhpStorm.
 * User: JAIR
 * Date: 4/27/2016
 * Time: 6:22 PM
 */

namespace Stevemo\Cpanel\User\Repo;

use Cartalyst\Sentry\Sentry;
use Cartalyst\Sentry\Users\UserInterface;
use Cartalyst\Sentry\Users\UserNotActivatedException;

/**
 * Class SentryUser
 * @package Stevemo\Cpanel\User\Repo
 */
class SentryUser extends Sentry
{
    /**
     * @param UserInterface $user
     * @param bool $remember
     */
    public function login(UserInterface $user, $remember = false)
    {
        if ( ! $user->isActivated())
        {
//            $login = $user->getLogin();
//            throw new UserNotActivatedException("Cannot login user [$login] as they are not activated.");
        }

        $this->user = $user;

        // Create an array of data to persist to the session and / or cookie
        $toPersist = array($user->getId(), $user->getPersistCode());

        // Set sessions
        $this->session->put($toPersist);

        if ($remember)
        {
            $this->cookie->forever($toPersist);
        }

        // The user model can attach any handlers
        // to the "recordLogin" event.
        $user->recordLogin();
    }
}