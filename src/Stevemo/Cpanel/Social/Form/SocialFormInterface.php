<?php namespace Stevemo\Cpanel\Social\Form;
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

/**
 * Interface SocialFormInterface
 * @package Stevemo\Cpanel\Social\Command
 */
interface SocialFormInterface {

    /**
     * create new social
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * destroy social
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * get all data social
     *
     * @param $userId
     * @return mixed
     */
    public function getSocialByUserId($userId);

}