<?php namespace Stevemo\Cpanel\Social\Repo;

/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

use Illuminate\Events\Dispatcher;

/**
 * Class SocialRepository
 * @package Stevemo\Cpanel\Social\Repo
 */
class SocialRepository implements SocialInterface
{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var Social
     */
    protected $model;
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var Dispatcher
     */
    protected $event;

    /**
     * @param Social $model
     * @param Dispatcher $event
     */
    function __construct(Social $model, Dispatcher $event)
    {
        $this->model = $model;
        $this->event = $event;
    }


    /**
     * store social
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $social = $this->model->create($data);

        if (!$social) {
            return false;
        } else {
            $this->event->fire('socials.create',array($social));
            return $social;
        }
    }

    /**
     * destroy social
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return Social::find($id)->delete();
    }

    /**
     * getAll social by userId
     *
     * @param $userId
     * @return mixed
     */
    public function getSocialByUserId($userId)
    {
        //TODO: implements
    }

}