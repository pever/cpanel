<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Stevemo\Cpanel\Mailers;

use Cartalyst\Sentry\Users\UserInterface as User;
use Mychef\User\Repo\User as Usuario;
use Lang;

/**
 * Class UserMailer
 * @package Stevemo\Cpanel\Mailers
 */
class UserMailer extends Mailer
{
    /**
     * send email confirmation user
     * @param User $user
     */
    public function sendConfirmationMessageTo(User $user)
    {
        $subject = Lang::get('email.welcome_casioli');

        $view = \Config::get('users::emails.confirmation');

        $data = array(
            "code" => $user->getActivationCode(),
            "userId" => $user->getId(),
            "full_name" => $user->full_name
        );

        $this->sendTo($user, $subject, $view, $data);
    }

    /**
     * send email activated user
     *
     * @param User $user
     */
    public function sendActivateMessageTo(User $user)
    {
        $subject = Lang::get('email.welcome_casioli');
        
        $view = \Config::get('users::emails.activated');

        $data = array(
            "full_name" => $user->full_name
        );

        $this->sendTo($user, $subject, $view,$data);
    }


    public function sendCodeActivateTo(User $user)
    {
        $subject = Lang::get('email.welcome_casioli');

        $view = \Config::get('users::emails.re_confirmation');

        $data = array(
            "code" => $user->activation_code,
            "userId" => $user->id,
            "full_name" => $user->full_name
        );

        $this->sendTo($user, $subject, $view, $data);
    }

    public function sendNewDevice($data)
    {
        $device = $data[0]; $user = $data[1];
        $usuario = Usuario::find($user['user_id']);
        //echo "<pre>"; print_r($user->toArray()); exit;

        $subject = Lang::get('email.logged_new_device');

        $view = 'emails.auth.new_device';

        $data = array(
            "device" => $device,
            "user" => $user,
            "usuario" => $usuario->toArray()
        );
        //echo "<pre>"; print_r($data); exit;
        //dd(\View::make($view, compact('device','user','usuario')));
        $this->sendTo($usuario, $subject, $view, $data);
    }
}