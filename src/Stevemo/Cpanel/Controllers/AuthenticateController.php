<?php namespace Stevemo\Cpanel\Controllers;

/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

use Illuminate\Support\Facades\Redirect;
use Stevemo\Cpanel\AuthSocial\FacebookInterface;
use Stevemo\Cpanel\AuthSocial\GoogleInterface;
use Stevemo\Cpanel\Social\Repo\SocialTypeNotFoundException;
use Stevemo\Cpanel\User\Repo\CpanelUserInterface;
use Config;
use Stevemo\Cpanel\User\Repo\UserNotFoundException;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;

/**
 * Class AuthenticateController
 * @package Stevemo\Cpanel\Controllers
 */
class AuthenticateController extends \BaseController
{

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var FacebookInterface
     */
    protected $facebook;
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var GoogleInterface
     */
    protected $google;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var CpanelUserInterface
     */
    protected $user;

    /**
     * @param FacebookInterface $facebook
     * @param GoogleInterface $google
     * @param CpanelUserInterface $userInterface
     */
    function __construct(FacebookInterface $facebook, GoogleInterface $google, CpanelUserInterface $userInterface)
    {
        $this->facebook = $facebook;
        $this->google = $google;
        $this->user = $userInterface;
    }

    //region FACEBOOK

    /**
     * @return mixed
     */
    public function authorizeFbRegister()
    {
        return $this->facebook->authorizeRegister();
    }

    /**
     * @return mixed
     */
    public function authorizeFbLogin()
    {
        return $this->facebook->authorizeLogin();
    }

    /**
     * @return mixed
     */
    public function registerFbCallback()
    {
        try {
            $code = \Input::get('code');

            $this->facebook->registerCallback($code);

            return \Redirect::route('cpanel.message.confirmation')
                ->with('success', \Lang::get('users::users.register_success'));

        } catch (FatalErrorException $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                ->with('error',$e->getMessage());
        } catch (\Exception $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                    ->with('error',$e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginFbCallback()
    {
        try {
            $code = \Input::get('code');

            $this->facebook->loginCallback($code);

            if($this->user->checkCompleted()){
                return \Redirect::intended(Config::get('users::prefix', 'admin'));
//                        ->with('success', \Lang::get('users::users.login_success'));
            }
            return \Redirect::route(Config::get('users::login_redirect', 'admin'));

        } catch (FatalErrorException $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                ->with('error',$e->getMessage());
        } catch (\Exception $e) {
            return \Redirect::route(\Lang::get('users::routes.login'))
                ->with('error',$e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function authorizeGgRegister()
    {
        return $this->google->authorizeRegister();
    }

    /**
     * @return mixed
     */
    public function authorizeGgLogin()
    {
        return $this->google->authorizeLogin();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerGgCallback()
    {
        try {
            $code = \Input::get('code');

            $this->google->registerCallback($code);

            return \Redirect::route('cpanel.message.confirmation')
                ->with('success', \Lang::get('users::users.register_success'));

        } catch (FatalErrorException $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                ->with('error',$e->getMessage());
        } catch (\Exception $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                ->with('error',$e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginGbCallback()
    {
        try {
            $code = \Input::get('code');

            $this->google->loginCallback($code);

            if($this->user->checkCompleted()){
                return \Redirect::intended(Config::get('users::prefix', 'admin'));
//                        ->with('success', \Lang::get('users::users.login_success'));
            }

            return \Redirect::route(Config::get('users::login_redirect', 'admin'));

        } catch (FatalErrorException $e) {
            return \Redirect::route(\Lang::get('users::routes.register'))
                ->with('error',$e->getMessage());
        } catch (\Exception $e) {
            return \Redirect::route(\Lang::get('users::routes.login'))
                ->with('error',$e->getMessage());
        }
    }

}