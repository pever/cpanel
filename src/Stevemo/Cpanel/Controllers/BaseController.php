<?php namespace Stevemo\Cpanel\Controllers;

use Controller;
use View;
use Config;
use Session;

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
        $app = \App::make('user_class');
        $user = $app->user;
        //share the config option to all the views
        View::share('cpanel', Config::get('users::site_config'));
        View::share('current_user', $user);
        if (Session::has('lang')) {
            $lang = '';
            if (Session::get('lang') != 'en') {
                $lang = '_'.Session::get('lang');
            }
            View::share('lang', $lang);
        } else {
            View::share('lang', '');
        }
    }

    /**
     * get the validation service
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @param  string $service
     * @param  array $inputs
     * @return Object
     */
    protected function getValidationService($service, array $inputs = array())
    {
        $class = '\\'.ltrim(Config::get("users::validation.{$service}"), '\\');
        return new $class($inputs);
    }

}
