<?php namespace Stevemo\Cpanel\Controllers;

use View, Config, Input, Redirect, Lang;
use Stevemo\Cpanel\User\Repo\CpanelUserInterface;
use Stevemo\Cpanel\User\Form\UserFormInterface;
use Mychef\Core\Traits\RedirectEspanolTrait;

class CpanelController extends BaseController {

    use RedirectEspanolTrait;

    /**
     * @var \Stevemo\Cpanel\User\Repo\CpanelUserInterface
     */
    private $users;

    /**
     * @var \Stevemo\Cpanel\User\Form\UserFormInterface
     */
    private $userForm;

    /**
     * @param CpanelUserInterface                         $users
     * @param \Stevemo\Cpanel\User\Form\UserFormInterface $userForm
     */
    public function __construct(CpanelUserInterface $users, UserFormInterface $userForm)
    {
        $this->beforeFilter('noSentryAuth', array('only'=>array('getLogin','getRegister','getAdmin','getLoginClean','getRegisterClean')));
        $this->beforeFilter('sentryAuth', array('only'=>array('getDashboard')));
        $this->users = $users;
        $this->userForm = $userForm;
    }


    /**
     * Show the dashboard
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return View::make(Config::get('users::views.dashboard'));
    }

    /**
     * Show the login form
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        $this->verifLang('login');
//        $val = $this->verifLanguage('login');
//        if($val != '') { return $val; }
        $login_attribute = Config::get('cartalyst/sentry::users.login_attribute');
        return View::make(Config::get('users::views.login'), compact('login_attribute'));
    }

    public function getLoginClean()
    {
        $this->verifLang('ingresar');
//        $val = $this->verifLocal('login');
//        if($val != '') { return $val; }
        $login_attribute = Config::get('cartalyst/sentry::users.login_attribute');
        return View::make(Config::get('users::views.login'), compact('login_attribute'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAdmin()
    {
        $login_attribute = Config::get('cartalyst/sentry::users.login_attribute');
        return View::make(Config::get('users::views.admin'), compact('login_attribute'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getDashboard()
    {
        return View::make(Config::get('users::views.dashboard'));
    }

    /**
     * Display the registration form
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        $this->verifLang('register');
//        $val = $this->verifLanguage('register');
//        if($val != '') { return $val; }
        $login_attribute = Config::get('cartalyst/sentry::users.login_attribute');
        return View::make(Config::get('users::views.register'), compact('login_attribute'));
    }

    public function getRegisterClean()
    {
        $this->verifLang('registrate');
//        $val = $this->verifLocal('register');
//        if($val != '') { return $val; }
        $login_attribute = Config::get('cartalyst/sentry::users.login_attribute');
        return View::make(Config::get('users::views.register'), compact('login_attribute'));
    }

    /**
     * Logs out the current user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        $this->users->logout();
        return Redirect::route('cpanel.home');
            //->with('success', Lang::get('users::users.logout'));
    }

    /**
     * Authenticate the user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin()
    {
        $remember = Input::get('remember_me', false);
        $userdata = array(
            Config::get('cartalyst/sentry::users.login_attribute') => Input::get('login_attribute'),
            'password' => Input::get('password')
        );

        if ( $this->userForm->login($userdata,$remember) )
        {
            if($this->users->checkCompleted()){
                return Redirect::intended(Config::get('users::prefix', 'admin'));
//                    ->with('success', Lang::get('users::users.login_success'));
            }

            return Redirect::route(Config::get('users::login_redirect', 'admin'));
        }

        return Redirect::back()
            ->withInput()
            ->with('login_error',$this->userForm->getErrors()->first());


    }

    /**
     * Register user
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRegister()
    {
        if( $this->userForm->register(Input::all(),false) )
        {
            return Redirect::route('cpanel.message.confirmation')
                ->with('success', Lang::get('users::users.register_success'));
        }

        return Redirect::back()
            ->withInput()
            ->withErrors($this->userForm->getErrors());

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getMessageConfirmation(){
        return Redirect::route('user.update');
//        return View::make(Config::get('users::views.message_confirmation'));
    }
}