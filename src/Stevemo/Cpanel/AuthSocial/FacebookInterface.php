<?php namespace Stevemo\Cpanel\AuthSocial;
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

/**
 * Interface FacebookInterface
 * @package Stevemo\Cpanel\AuthSocial
 */
/**
 * Interface FacebookInterface
 * @package Stevemo\Cpanel\AuthSocial
 */
interface FacebookInterface {


    /**
     * @return mixed
     */
    public function authorizeLogin();

    /**
     * @return mixed
     */
    public function authorizeRegister();

    /**
     * callback login to facebook auth
     * @param $code
     * @return mixed
     */
    public function loginCallback($code);


    /**
     * callback to register facebook auth
     *
     * @param $code
     * @return mixed
     */
    public function registerCallback($code);

    /**
     * Get the validation errors
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return array
     */
    public function getErrors();
}