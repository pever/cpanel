<?php namespace Stevemo\Cpanel\AuthSocial;

/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\UserExistsException;
use Stevemo\Cpanel\Services\Validation\ValidableInterface;
use Stevemo\Cpanel\AuthSocial\Password\RandomInterface;
use Stevemo\Cpanel\Social\Repo\SocialNotFoundException;
use Stevemo\Cpanel\Social\Repo\SocialTypeNotFoundException;
use Stevemo\Cpanel\User\Repo\CpanelUserInterface;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSDKException;
use Facebook\GraphUser;
use Stevemo\Cpanel\User\Repo\UserNotFoundException;
use Config;

/**
 * Class FacebookAuth
 * @package Stevemo\Cpanel\AuthSocial
 */
class FacebookAuth implements FacebookInterface
{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    private static $TYPE_FACEBOOK = 'facebook';

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var CpanelUserInterface
     */
    protected $userRepo;


    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var ValidableInterface
     */
    protected $validator;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var RandomInterface
     */
    protected $random;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $helper;

    /**
     * @param ValidableInterface $validator
     * @param CpanelUserInterface $userRepo
     * @param RandomInterface $random
     */
    function __construct(ValidableInterface $validator,
                         CpanelUserInterface $userRepo,
                         RandomInterface $random)
    {
        $this->userRepo = $userRepo;
        $this->validator = $validator;
        $this->random = $random;
    }

    /**
     * @return mixed
     */
    public function authorizeLogin()
    {
        $this->helper = new LaravelFacebookRedirectLoginHelper(route('cpanel.facebook.login.callback'));
        return \Redirect::to($this->helper->getLoginUrl(array('scope' => Config::get('users::social_permission.facebook'))));
    }

    /**
     * @return mixed
     */
    public function authorizeRegister()
    {
        $this->helper = new LaravelFacebookRedirectLoginHelper(route('cpanel.facebook.register.callback'));
        return \Redirect::to($this->helper->getLoginUrl(array('scope' => Config::get('users::social_permission.facebook'))));
    }

    /**
     * callback login to facebook auth
     *
     * @param $code
     * @return mixed
     */
    public function loginCallback($code)
    {
        try {
            if (strlen($code) == 0) {
                throw new \Exception(\Lang::get('users::users.facebook_failed'));
            }

            $helper = new LaravelFacebookRedirectLoginHelper(route('cpanel.facebook.login.callback'),
                getenv('FACEBOOK_CLIENT_ID'), getenv('FACEBOOK_CLIENT_SECRET'));

            $session = $helper->getSessionFromRedirect();

            if ($session == null) {
                throw new \Exception (\Lang::get('users::users.facebook_failed'));
            }

            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();

            $user = $response->getGraphObject(GraphUser::className());
            $this->authenticateFacebook($user);

        } catch (FacebookRequestException $e) {
            throw new \Exception(\Lang::get('users::users.facebook_failed'));
        } catch (FacebookSDKException $e)  {
            throw new \Exception(\Lang::get('users::users.facebook_failed'));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * callback to register facebook auth
     *
     * @param $code
     * @return mixed
     */
    public function registerCallback($code)
    {
        try {
            if (strlen($code) == 0) {
                throw new \Exception(\Lang::get('users::users.facebook_failed'));
            }

            $helper = new LaravelFacebookRedirectLoginHelper(route('cpanel.facebook.register.callback'),
                getenv('FACEBOOK_CLIENT_ID'), getenv('FACEBOOK_CLIENT_SECRET'));

            $session = $helper->getSessionFromRedirect();

            if ($session == null) {
                throw new \Exception (\Lang::get('users::users.facebook_failed'));
            }

            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();

            $user = $response->getGraphObject(GraphUser::className());

            $this->saveUser($user);

        } catch (FacebookRequestException $e) {
            throw new \Exception(\Lang::get('users::users.facebook_failed'));
        } catch (FacebookSDKException $e)  {
            throw new \Exception(\Lang::get('users::users.facebook_failed'));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * store user
     *
     * @param $user
     * @return bool
     */
    private function saveUser($user)
    {
        try {
            $activate = false;

            $credentials = array(
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
                'email' => $user->getEmail(),
                'password' => $this->random->generateRandom(),
                'password_confirmation' => $this->random->getValue(),
                '_token' => csrf_token()
            );
            if ($this->validator->with($credentials)->passes()) {
                $this->userRepo->registerUserFacebook($user, $credentials, $activate);
                $this->authenticateFacebook($user);
            } else {
                throw new \Exception (\Lang::get('users::users.data_invalid'));
            }

        } catch (UserExistsException $e) {
            $message = \Lang::get('users::users.user_exists', array('email' => $user->getEmail()));
            throw new \Exception ($message);
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }


    /**
     * @param $user
     * @return mixed
     */
    private function authenticateFacebook($user)
    {
        $email = $user->getEmail();

        try {

            $userSentry = $this->userRepo->findByEmail($email);

            $typeSocial = $userSentry->social->type;

            if ($typeSocial != self::$TYPE_FACEBOOK) {
                $msg = \Lang::get('users::users.social_exists', array('email' => $email, 'typeSocial' => $typeSocial));
                throw new \Exception ($msg);
            }
//            $this->login($userSentry);
            $this->userRepo->login($userSentry);

        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            throw new \Exception (\Lang::get('users::users.not_found', array('email' => $email)));
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $sentryUser
     * @return bool
     */
    private function login($sentryUser)
    {
        try {
            if ($this->userRepo->login($sentryUser)) {
                return true;
            }
        } catch (LoginRequiredException $e) {
            $this->validator->add('LoginRequiredException', $e->getMessage());
        } catch (UserNotActivatedException $e) {
            $this->validator->add('UserNotActivatedException', $e->getMessage());
        } catch (UserNotFoundException $e) {
            $this->validator->add('UserNotFoundException', $e->getMessage());
        } catch (UserSuspendedException $e) {
            $this->validator->add('UserSuspendedException', $e->getMessage());
        } catch (UserBannedException $e) {
            $this->validator->add('UserBannedException', $e->getMessage());
        }
        return false;
    }

    /**
     * Get the validation errors
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->validator->errors();
    }


}