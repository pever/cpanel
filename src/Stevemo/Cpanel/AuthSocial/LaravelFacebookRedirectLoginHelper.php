<?php namespace Stevemo\Cpanel\AuthSocial;

use Facebook\FacebookRedirectLoginHelper;

/**
 * Class LaravelFacebookRedirectLoginHelper
 * @package Stevemo\Cpanel\AuthSocial
 */
class LaravelFacebookRedirectLoginHelper extends FacebookRedirectLoginHelper
{

    /**
     * @param string $state
     */
    protected function storeState($state)
    {
        \Session::put('state', $state);
    }

    /**
     * @return mixed
     */
    protected function loadState()
    {
        return $this->state = \Session::get('state');
    }

}
