<?php namespace Stevemo\Cpanel\AuthSocial\Password;
    /*
     *
     *  * Copyright (C) 2015 eveR Vásquez.
     *  *
     *  * Licensed under the Apache License, Version 2.0 (the "License");
     *  * you may not use this file except in compliance with the License.
     *  * You may obtain a copy of the License at
     *  *
     *  *      http://www.apache.org/licenses/LICENSE-2.0
     *  *
     *  * Unless required by applicable law or agreed to in writing, software
     *  * distributed under the License is distributed on an "AS IS" BASIS,
     *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     *  * See the License for the specific language governing permissions and
     *  * limitations under the License.
     *
     */

/**
 * Class RandomPassword
 * @package Stevemo\Cpanel\AuthSocial\Password
 */
class RandomPassword implements RandomInterface
{

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    private $value;


    /**
     * Generate random input to output
     *
     * @return string
     */
    public function generateRandom()
    {
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $cad = "";
        for ($i = 0; $i < 6; $i++) {
            $cad .= substr($str, rand(0, 62), 1);
        }
        return $this->value = $cad;
    }

    /**
     * return value
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }


}