<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Stevemo\Cpanel\AuthSocial;


/**
 * Class GoogleAuth
 * @package Stevemo\Cpanel\AuthSocial
 */
use Cartalyst\Sentry\Users\UserExistsException;
use Stevemo\Cpanel\Services\Validation\ValidableInterface;
use Stevemo\Cpanel\AuthSocial\Password\RandomInterface;
use Stevemo\Cpanel\Social\Repo\SocialNotFoundException;
use Stevemo\Cpanel\Social\Repo\SocialTypeNotFoundException;
use Stevemo\Cpanel\User\Repo\CpanelUserInterface;
use Stevemo\Cpanel\User\Repo\UserNotFoundException;
use Config;

/**
 * Class GoogleAuth
 * @package Stevemo\Cpanel\AuthSocial
 */
class GoogleAuth implements GoogleInterface
{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    public static $TYPE_GOOGLE = 'google';
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $client;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var CpanelUserInterface
     */
    protected $userRepo;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var ValidableInterface
     */
    protected $validator;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var RandomInterface
     */
    protected $random;


    /**
     * @param ValidableInterface $validator
     * @param CpanelUserInterface $userRepo
     * @param RandomInterface $random
     */
    function __construct(ValidableInterface $validator, CpanelUserInterface $userRepo, RandomInterface $random)
    {
        $this->validator = $validator;
        $this->random = $random;
        $this->userRepo = $userRepo;
    }

    /**
     * login to google auth
     *
     * @return mixed
     */
    public function authorizeRegister()
    {
        $this->initRegister();
        return \Redirect::to($this->getAuthUrl());
    }

    /**
     * @return mixed
     */
    public function authorizeLogin()
    {
        $this->initLogin();
        return \Redirect::to($this->getAuthUrl());
    }

    /**
     * callback to google auth
     *
     * @param $code
     * @return mixed
     */
    public function registerCallback($code)
    {
        try {
            if (isset($code)) {
                $this->initRegister();
                $this->client->authenticate($code);
                $token = $this->client->getAccessToken();
                \Session::put('token', $token);
            } else {
                throw new \Exception (\Lang::get('users::users.google_failed'));
            }

            if ($this->isLoggedIn()) {

                $oauth = new \Google_Service_Oauth2($this->client);

                $user = $oauth->userinfo->get();

                $this->saveUser($user);

            } else {
                return \Redirect::to($this->getAuthUrl());
            }
        } catch (\Exception $e) {
           throw new \Exception ($e->getMessage());
        }
    }

    /**
     * @param $code
     * @return mixed
     */
    public function loginCallback($code)
    {
        try {
            if (isset($code)) {
                $this->initLogin();
                $this->client->authenticate($code);
                $token = $this->client->getAccessToken();
                \Session::put('token', $token);
            } else {
                throw new \Exception (\Lang::get('users::users.google_failed'));
            }

            if ($this->isLoggedIn()) {

                $oauth = new \Google_Service_Oauth2($this->client);

                $user = $oauth->userinfo->get();

                $this->authenticateGoogle($user);

            } else {
                return \Redirect::to($this->getAuthUrl());
            }

        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }


    /**
     * @param $oauth
     * @return bool
     * @throws SocialTypeNotFoundException
     */
    private function authenticateGoogle($oauth)
    {
        $email = $oauth->email;

        try {
            $userSentry = $this->userRepo->findByEmail($email);
            $typeSocial = $userSentry->social->type;

            if ($typeSocial != self::$TYPE_GOOGLE) {
                $msg = \Lang::get('users::users.social_exists', array('email' => $email, 'typeSocial' => $typeSocial));
                throw new \Exception ($msg);
            }
//            $this->login($userSentry);
            $this->userRepo->login($userSentry);

        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            throw new \Exception (\Lang::get('users::users.not_found', array('email' => $email)));
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    //region INITIALIZE

    /**
     * initialize method register
     */
    private function initRegister()
    {
        $this->client = new \Google_Client();
        $this->client->setClientId(getenv('GOOGLE_CLIENT_ID'));
        $this->client->setClientSecret(getenv('GOOGLE_CLIENT_SECRET'));
        $this->client->setRedirectUri(route('cpanel.google.register.callback'));
        $this->client->setScopes(Config::get('users::social_permission.google'));
    }

    /**
     * initialize method login
     */
    private function initLogin()
    {
        $this->client = new \Google_Client();
        $this->client->setClientId(getenv('GOOGLE_CLIENT_ID'));
        $this->client->setClientSecret(getenv('GOOGLE_CLIENT_SECRET'));
        $this->client->setRedirectUri(route('cpanel.google.login.callback'));
        $this->client->setScopes(Config::get('users::social_permission.google'));
    }
    //endregion

    /**
     * get url to redirect
     * @return string
     */
    private function  getAuthUrl()
    {
        return $this->client->createAuthUrl();
    }

    /**
     * verify is logged
     * @return bool|string
     */
    private function isLoggedIn()
    {
        if (\Session::has('token')) {
            $this->client->setAccessToken(\Session::get('token'));
            return true;
        }
        return $this->client->getAccessToken();
    }

    /**
     * @param $googleUser
     * @return bool
     */
    private function saveUser($googleUser)
    {
        try {
            $activate = false;

            $credentials = array(
                'first_name' => $googleUser->givenName,
                'last_name' => $googleUser->familyName,
                'email' => $googleUser->email,
                //'picture' => $googleUser->picture,
                //'google_id' => $googleUser->id,
                'password' => $this->random->generateRandom(),
                'password_confirmation' => $this->random->getValue(),
                '_token' => csrf_token()
            );
            if ($this->validator->with($credentials)->passes()) {
                $this->userRepo->registerUserGoogle($googleUser, $credentials, $activate);
                $this->authenticateGoogle($googleUser);
            } else {
                throw new \Exception (\Lang::get('users::users.data_invalid'));
            }

        } catch (UserExistsException $e) {
            $message = \Lang::get('users::users.user_exists', array('email' => $googleUser->email));
            throw new \Exception ($message);
        } catch (\Exception $e) {
            throw new \Exception ($e->getMessage());
        }
    }

    /**
     * logout to google
     * @return mixed
     */
    public function logout()
    {
        \Session::forget('token');
    }

    /**
     * @param $sentryUser
     * @return bool
     */
    private function login($sentryUser)
    {
        try {
            if ($this->userRepo->login($sentryUser)) {
                return true;
            }
        } catch (LoginRequiredException $e) {
            $this->validator->add('LoginRequiredException', $e->getMessage());
        } catch (UserNotActivatedException $e) {
            $this->validator->add('UserNotActivatedException', $e->getMessage());
        } catch (UserNotFoundException $e) {
            $this->validator->add('UserNotFoundException', $e->getMessage());
        } catch (UserSuspendedException $e) {
            $this->validator->add('UserSuspendedException', $e->getMessage());
        } catch (UserBannedException $e) {
            $this->validator->add('UserBannedException', $e->getMessage());
        }
        return false;
    }


    /**
     * Get the validation errors
     *
     * @author Steve Montambeault
     * @link   http://stevemo.ca
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->validator->errors();
    }


}