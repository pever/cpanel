<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
namespace Stevemo\Cpanel\Events;

use Stevemo\Cpanel\Mailers\UserMailer;

class EmailNotifier
{
    /**
     * @var UserMailer
     */
    private $mailer;

    /**
     * @param UserMailer $mailer
     */
    public function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send a welcome email to the new user
     *
     * @param User $user
     * @return void
     */
    public function handle($user)
    {
        // Send the welcome email
    }

    public function subscribe($events)
    {
        $events->listen('users.register', 'Stevemo\Cpanel\Events\EmailNotifier@sendMessageRegister');
        $events->listen('users.activate', 'Stevemo\Cpanel\Events\EmailNotifier@sendMessageActivate');
        $events->listen('users.sendCode', 'Stevemo\Cpanel\Events\EmailNotifier@sendCodeActivate');
        $events->listen('users.newDevice', 'Stevemo\Cpanel\Events\EmailNotifier@sendNewDevice');
    }

    /**
     * @param $user
     */
    public function sendMessageRegister($user)
    {
        $this->mailer->sendConfirmationMessageTo($user);
    }

    /**
     * @param $user
     */
    public function sendMessageActivate($user)
    {
        $this->mailer->sendActivateMessageTo($user);
    }
/**
     * @param $user
     */
    public function sendCodeActivate($user)
    {
        $this->mailer->sendCodeActivateTo($user);
    }
/**
     * @param $user
     */
    public function sendNewDevice($user)
    {
        $this->mailer->sendNewDevice($user);
    }

}