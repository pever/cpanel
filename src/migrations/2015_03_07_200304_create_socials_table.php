<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('socials', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('type'); //facebook or google
			$table->string('social_id');
			$table->string('picture')->nullable();
			$table->text('url')->nullable();
			$table->text('extra')->nullable();
			$table->timestamp('published_at')->nullable();
			$table->tinyInteger('published')->default(true);
			$table->timestamps();
			$table->softDeletes();
		});
		//socials
		Schema::table('socials',function(Blueprint $table){
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		//socials
		Schema::table('socials',function(Blueprint $table){
			$table->dropForeign('socials_users_id');
		});
		Schema::drop('socials');
	}

}
