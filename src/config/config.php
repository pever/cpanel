<?php

return array(

    // route prefix
    'prefix' => 'admin',

    'login_redirect' => 'user.update',

    'users' => array(
        'login_success' => ''
    ),

    'social_permission' =>array(
        "facebook" => "public_profile,email,user_friends,user_birthday,user_hometown,user_education_history,user_likes,user_location",
        "google" => array(
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        )
    ),

    //Generic Permissions
    'generic_permission' => array('view', 'create', 'update', 'delete'),

    'site_config' => array(
        'site_name' => 'Cpanel',
        'title' => 'My Admin Panel',
        'description' => 'Laravel 4 Admin Panel'
    ),

    //emails
    'emails' => array(
        'confirmation' => 'users::emails.confirmation',
        'activated' => 'users::emails.activated'
    ),

    //menu 2 type are available single or dropdown and it must be a route
    'menu' => array(
        'Dashboard' => array('type' => 'single', 'route' => 'cpanel.home'),
        'Users' => array('type' => 'dropdown', 'links' => array(
            'Manage Users' => array('route' => 'cpanel.users.index'),
            'Groups' => array('route' => 'cpanel.groups.index'),
            'Permissions' => array('route' => 'cpanel.permissions.index')
        )),
    ),

    'views' => array(

        'layout' => 'users::layouts',

        'dashboard' => 'users::dashboard.index',
        'admin' => 'users::dashboard.admin',
        'login' => 'users::dashboard.login',
        'register' => 'users::dashboard.register',

        // Users views
        'users_index' => 'users::users.index',
        'users_show' => 'users::users.show',
        'users_edit' => 'users::users.edit',
        'users_create' => 'users::users.create',
        'users_permission' => 'users::users.permission',

        //Groups Views
        'groups_index' => 'users::groups.index',
        'groups_create' => 'users::groups.create',
        'groups_edit' => 'users::groups.edit',
        'groups_permission' => 'users::groups.permission',

        //Permissions Views
        'permissions_index' => 'users::permissions.index',
        'permissions_edit' => 'users::permissions.edit',
        'permissions_create' => 'users::permissions.create',

        //Throttling Views
        'throttle_status' => 'users::throttle.index',

        //password Views
        'password_forgot' => 'users::password.forgot',
        'email_password_forgot' => 'users::password.email',
        'password_send' => 'users::password.send',
        'password_reset' => 'users::password.reset',

        //message Views
        'message_confirmation' => 'users::password.send',
    ),

    'validation' => array(
        'user' => 'Stevemo\Cpanel\Services\Validators\Users\Validator',
        'permission' => 'Stevemo\Cpanel\Services\Validators\Permissions\Validator',
    ),
);
