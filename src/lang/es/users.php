<?php

return array(
    'logout'                     => 'Sesión finalizada.',
    'register_success'           => '¡Bienvenido! Ya estás registrado.',
    'create_success'             => 'Usuario creado.',
    'update_success'             => 'Usuario modificado.',
    'login_success'              => 'Has iniciado una nueva sesión.',
    'delete_denied'              => 'Lo sentimos, no está permitido borrar un usuario desde su propia cuenta, solicite el borrado al administrador.',
    'delete_success'             => 'Usuario borrado.',
    'permissions_update_success' => 'Modificados permisos del usuario.',
    'activation_success'         => 'Su email se activó correctamente.',
    'activation_fail'            => 'No se pudo activar su email. Esto puede ser debido a que la ruta ingresada no es valida. Por favor vuelva a enviar un email de verificación.',
    'deactivation_success'       => 'Usuario desactivado correctamente.',
    'password_reset_success'     => 'Password reset successfully',
    'code_activation_expired'    => 'Su código de activación ha expirado. Por favor, envíe un email de confirmación para volver a recibir el mensaje.',
    'device_not_allowed'         => 'No se le permite conectarse desde este dispositivo',
    'not_found'                  => 'No se ha encontrado usuario con el email [:email].',
    'is_active'                  => 'Éste usuario ya está activado',
    'send_confirmation'          => 'Se re-envío el email de confirmación. Confirme para verificar su email',
    'email_not_register'         => 'El usuario con email :email no está registrado',
    'social_not_found'           => 'El email :email ya esta registrado con otro tipo de cuenta',
    'not_activated'              => 'No puede ingresar porque esa cuenta no ha sido activada, por favor <a href=":link">registrate aqui</a>',
    'user_exists'                => 'Ya existe el usuario [:email], los logins son únicos.',
    'facebook_failed'            => 'Hubo un problema al comunicarse con Facebook. Intente nuevamente.',
    'data_invalid'               => 'Los datos proporcionados no son validos. Intente nuevamente.',
    'google_failed'              => 'Hubo un problema al comunicarse con Google. Intente nuevamente.'
);
