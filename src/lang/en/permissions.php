<?php

return array(
   'no_found'        => 'No Modules Permissions Found.',
   'create_success'  => 'Permissions created.',
   'model_not_found' => 'These permissions do not exist.',
   'update_success'  => 'Permissions updated.',
   'delete_success'  => 'Modules Permissions deleted.',
   'no_permission'   => 'No Permissions found.',
   'access_denied'   => 'You do not have sufficient permissions to view this page.',
);
